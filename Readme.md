## Run a container of openFoam to run simulations

```bash
docker container run -ti --rm   -v $PWD:/data -w /data openfoam:v7
```




```bash
# save
docker save openfoam:v7 | gzip > openfoam7_docker.tar.gz


# load 

docker load < openfoam7_docker.tar.gz
```



```
docker container run -ti --rm  --cpus 8 -v $PWD:/data -w /data openfoam:v7
```



```

docker run --rm  -u root --cpus 8 -v $PWD:/data  openfoam:v7  bash -c 'source /opt/openfoam7/etc/bashrc; blockMesh'

```
