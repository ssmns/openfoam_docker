FROM ubuntu:20.04

LABEL maintainer="ssmns ssmns@outlook.com"
LABEL version="1.0-beta"
LABEL description="This is custom Docker Image for openfoam version 7 package."


ENV DEBIAN_FRONTEND noninteractive


# ENV TZ=Asia/Tehran
# RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update; \
    apt-get install -y --no-install-recommends \
    git \
    make \
    wget \ 
    gfortran \
    build-essential \ 
    unzip \
    nano 

RUN apt-get install  --no-install-recommends  --no-install-suggests -y curl ca-certificates \
    && mkdir /app 
# Install openfoam7
COPY requirments/openfoam7_20200508_amd64.deb /app
RUN cd /app  \
    && apt install -y --no-install-recommends ./openfoam7_20200508_amd64.deb \
    && apt-get install -f \
    && echo "source /opt/openfoam7/etc/bashrc" >> /etc/profile  \
    && echo "export OMPI_MCA_btl_vader_single_copy_mechanism=none" >> /etc/profile  \
    && apt autoclean -y \
    && apt autoremove -y  \
    && rm -rf /app/openfoam7_20200508_amd64.deb

# # Install SimpleFoamT
SHELL ["/bin/bash", "-c"] 
RUN cd /app \
    && wget https://codeberg.org/ssmns/SimpleFoamT/archive/master.tar.gz \
    && tar -xvf master.tar.gz \
    && cd simplefoamt \ 
    && source /opt/openfoam7/etc/bashrc \
    && wmake \ 
    && cd ..\
    && rm -rf master.tar.gz simplefoamt \
    && rm -rf /var/lib/apt/lists/*  
# change environmental variables to make sure $WM_PROJECT_USER_DIR is outside of the container
# RUN sed -i '/export WM_PROJECT_USER_DIR=/cexport WM_PROJECT_USER_DIR="/data/foam-$WM_PROJECT_VERSION"' /usr/lib/openfoam/openfoam/etc/bashrc

RUN echo "source /opt/openfoam7/etc/bashrc" >> /root/.bashrc  \
    && echo "export OMPI_MCA_btl_vader_single_copy_mechanism=none" >> /root/.bashrc

WORKDIR /data

# COPY requirments/entrypoint.sh /entrypoint.sh
# RUN chmod +x /entrypoint.sh

# ENTRYPOINT ["/bin/bash","-c","source /opt/openfoam7/etc/bashrc;"]
# CMD [ "bash" ]